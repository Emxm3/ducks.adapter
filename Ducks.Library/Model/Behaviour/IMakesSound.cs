﻿using Ducks.Library.Model.Strategies.Sound;

namespace Ducks.Library.Model.Behaviour
{
    public interface IMakesSound
    {
        ISoundStrategy SoundStrategy { get; }
        public string Sound { get; }
    }
}
