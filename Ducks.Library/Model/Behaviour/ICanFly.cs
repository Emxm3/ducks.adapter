﻿
using Ducks.Library.Model.Strategies.Flight;

namespace Ducks.Library.Model.Behaviour

{
    public interface ICanFly
    {
        IFlightStrategy FlightStrategy { get; }
        string FlightDescription { get; }
    }
}
