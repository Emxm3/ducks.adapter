﻿
using Ducks.Library.Observable;
using System.ComponentModel;

namespace Ducks.Library.Model.Containers
{
    public abstract class Container<T> : ObservableClass, IContainer<T> where T : class
    {
        private T _Value;
        public T Value { get => _Value; set => SetPropertyValue(ref _Value, value); }
    }

    public interface IContainer<T> : INotifyPropertyChanged where T : class
    {
        T Value { get; set; }
    }
}
