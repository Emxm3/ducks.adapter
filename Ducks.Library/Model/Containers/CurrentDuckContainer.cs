﻿using Ducks.Library.Attributes;
using Ducks.Library.Model;
using Ducks.Library.Model.Containers;

namespace Ducks.Adapter.MVVM.Model.Containers
{
    [AutoWire(AsSingleton:true)]
    public class CurrentDuckContainer : Container<IDuck>, ICurrentDuckContainer{}

    public interface ICurrentDuckContainer : IContainer<IDuck>{}
}
