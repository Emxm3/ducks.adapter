﻿
using Ducks.Library.Observable;
using System;

namespace Ducks.Library.Model
{
    

    public abstract class Duck : ObservableClass, IDuck
    {

        public string Name { get; protected set; }
        public string Breed { get; protected set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }

    public interface IDuck : ICloneable
    {
        string Breed { get; }
        string Name { get; }

    }
}
