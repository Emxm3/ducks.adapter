﻿using Ducks.Library.Model;
using Ducks.Library.Patterns.Factory;

namespace Ducks.Library.Model.Ponds
{
    public class Lake : Pond
    {
        private readonly IDuckFactory factory;
        public Lake(IDuckFactory factory)
        {
            this.factory = factory;
        }
        public override IDuck BreedDuck()
        {
            return factory.Get();
        }

    }
}
