﻿using Ducks.Library.Model.Ducks;
using Ducks.Library.Patterns.Factory;

namespace Ducks.Library.Model.Ponds
{
    //Dip is trying to be smart. It is using Dependency Injection to determine what will get built
    public class Dip : Pond
    {
        private readonly IDuckFactory factory;

        public Mallard TargetDuck { get; }

        public Dip(IDuckFactory factory)
        {
            this.factory = factory;
        }

        public override IDuck BreedDuck()
        {
            return factory.Get();
        }

    }
}
