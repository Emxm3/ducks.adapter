﻿using Ducks.Library.Model;
using Ducks.Library.Patterns.Factory;

namespace Ducks.Library.Model.Ponds
{
    public class River : Pond
    {
        private readonly IDuckFactory factory;

        public River(IDuckFactory factory)
        {
            this.factory = factory;
        }

        public override IDuck BreedDuck()
        {

            IDuck duck = factory.Get();

            return duck;
        }
    }
}
