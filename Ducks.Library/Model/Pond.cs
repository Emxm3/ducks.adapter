﻿
using Ducks.Library.Observable;
using Ducks.Library.Patterns.Command;
using System;
using System.Windows.Input;

namespace Ducks.Library.Model.Ponds
{


    public abstract class Pond : ObservableClass, IPond
    {
        public event EventHandler<IDuck> EmitDuck;


        public ICommand SendDuck => new RelayCommand(() => _SendDuck());

        private void _SendDuck()
        {
            IDuck bredDuck = BreedDuck();
            EmitDuck?.Invoke(this, bredDuck);
        }

        public string Name => GetType().Name;

        public abstract IDuck BreedDuck();

    }

    public interface IPond
    {
        string Name { get; }
        ICommand SendDuck { get; }
        event EventHandler<IDuck> EmitDuck;
        IDuck BreedDuck();
    }

}
