﻿namespace Ducks.Library.Model.Ducks
{
    public class PekingDuck : Duck
    {

        public PekingDuck()
        {
            Name = "Peking Duck";
        }

    }
}
