﻿using Ducks.Library.Model.Behaviour;
using Ducks.Library.Model.Strategies.Sound;

namespace Ducks.Library.Model.Ducks
{
    public class DecoyDuck : Duck, IMakesSound
    {
        private string _Sound;
        public string Sound { get => _Sound ??= SoundStrategy?.Execute(); set => SetPropertyValue(ref _Sound, value); }

        public DecoyDuck(IQuackSoundStrategy quackSoundStrategy)
        {
            Name = "Decoy Duck";
            SoundStrategy = quackSoundStrategy;
        }

        public ISoundStrategy SoundStrategy { get; }

       
    }
}
