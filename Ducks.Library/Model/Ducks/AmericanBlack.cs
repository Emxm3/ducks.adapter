﻿using Ducks.Library.Model.Behaviour;
using Ducks.Library.Model.Strategies.Flight;
using Ducks.Library.Model.Strategies.Sound;

namespace Ducks.Library.Model.Ducks
{
    public class AmericanBlackDuck : Duck, ICanFly, IMakesSound
    {
        private string _FlightDescription;
        public string FlightDescription { get => _FlightDescription ??= FlightStrategy?.Execute(); set => SetPropertyValue(ref _FlightDescription, value); }


        private string _Sound;
        public string Sound { get => _Sound ??= SoundStrategy?.Execute(); set => SetPropertyValue(ref _Sound, value); }

        public AmericanBlackDuck(IWingedFlightStrategy wingedFlightStrategy, IQuackSoundStrategy quackSoundStrategy)
        {
            Name = "American Black Duck";
            FlightStrategy = wingedFlightStrategy;
            SoundStrategy = quackSoundStrategy;
        }

        public IFlightStrategy FlightStrategy { get; }
        public ISoundStrategy SoundStrategy { get; }

        IFlightStrategy ICanFly.FlightStrategy => throw new System.NotImplementedException();
    }
}
