﻿using Ducks.Library.Model.Behaviour;
using Ducks.Library.Model.Strategies.Flight;
using Ducks.Library.Model.Strategies.Sound;

namespace Ducks.Library.Model.Ducks
{
    public class Muscovy : Duck, ICanFly, IMakesSound
    {
        private string _FlightDescription;
        public string FlightDescription { get => _FlightDescription ??= FlightStrategy?.Execute(); set => SetPropertyValue(ref _FlightDescription, value); }

        private string _Sound;
        public string Sound { get => _Sound ??= SoundStrategy?.Execute(); set => SetPropertyValue(ref _Sound, value); }

        public Muscovy(IWingedFlightStrategy wingedFlightStrategy, IQuackSoundStrategy quackSoundStrategy)
        {
            Name = "Muscovy";
            FlightStrategy = wingedFlightStrategy;
            SoundStrategy = quackSoundStrategy;
        }

        public IFlightStrategy FlightStrategy { get; }
        public ISoundStrategy SoundStrategy { get; }
    }
}
