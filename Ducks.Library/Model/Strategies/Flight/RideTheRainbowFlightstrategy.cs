﻿using Ducks.Library.Attributes;

namespace Ducks.Library.Model.Strategies.Flight
{
    [AutoWire(AsSingleton:true)]
    public class RideTheRainbowFlightStrategy : IRideTheRainbowFlightStrategy, IWingedFlightStrategy
    {
        public string Execute()
        {
            return "RIDE THE RAINBOW!!!";
        }
    }

    public interface IRideTheRainbowFlightStrategy : IFlightStrategy
    {
    }
}
