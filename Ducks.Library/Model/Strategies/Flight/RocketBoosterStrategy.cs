﻿using Ducks.Library.Attributes;

namespace Ducks.Library.Model.Strategies.Flight
{
    [AutoWire(AsSingleton: true)]
    public class RocketBoosterStrategy : IRocketBoosterStrategy
    {
        public string Execute()
        {

            return "Uses jet engines";
        }
    }

    public interface IRocketBoosterStrategy : IFlightStrategy
    {
    }
}
