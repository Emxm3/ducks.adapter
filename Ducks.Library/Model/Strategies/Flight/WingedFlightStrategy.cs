﻿using Ducks.Library.Attributes;

namespace Ducks.Library.Model.Strategies.Flight
{
    [AutoWire(AsSingleton: true)]
    public class WingedFlightStrategy : IWingedFlightStrategy
    {
        public string Execute() => "This duck flaps its wings";
    }


    public interface IWingedFlightStrategy : IFlightStrategy
    {
    }
}
