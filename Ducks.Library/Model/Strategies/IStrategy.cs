﻿namespace Ducks.Library.Model.Strategies
{
    public interface IStrategy
    {
        public string Execute();
    }
}