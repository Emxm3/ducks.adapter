﻿using Ducks.Library.Attributes;

namespace Ducks.Library.Model.Strategies.Sound
{
    [AutoWire(AsSingleton: true)]
    public class QuackSoundStrategy : IQuackSoundStrategy
    {
        public string Execute() => "This duck quacks";
    }

    public interface IQuackSoundStrategy : ISoundStrategy
    {
    }
}
