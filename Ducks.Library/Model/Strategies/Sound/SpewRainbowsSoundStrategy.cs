﻿namespace Ducks.Library.Model.Strategies.Sound
{
    public class SpewRainbowsSoundStrategy : ISpewRainbowsSoundStrategy, IQuackSoundStrategy
    {
        public string Execute()
        {
            return "** !!SPEWS RAINBOWS!! **";
        }
    }

    public interface ISpewRainbowsSoundStrategy : ISoundStrategy
    {
    }
}
