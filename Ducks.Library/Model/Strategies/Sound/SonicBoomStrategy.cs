﻿using Ducks.Library.Attributes;

namespace Ducks.Library.Model.Strategies.Sound
{
    [AutoWire(AsSingleton: true)]
    public class SonicBoomStrategy : ISonicBoomStrategy
    {
        public string Execute() => "SONIC BOOM!!";
    }

    public interface ISonicBoomStrategy : ISoundStrategy
    {
    }
}
