﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Ducks.Library.Observable
{
    public abstract class ObservableClass : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public void SetPropertyValue<T>(ref T currentValue, T newValue, Action callback = null, [CallerMemberName] string name = "")
        {

            //If there is no change, bail

            //First check if both null
            if (currentValue == null && newValue == null) return;

            //Next, check if the current value isn't null before trying to perform an Equals()
            if (currentValue != null && currentValue.Equals(newValue)) return;

            //Change the value and let the subscribers know a change took place
            currentValue = newValue;
            NotifyPropertyChanged(name);

            //once done, run the callback
            callback?.Invoke();
        }
    }
}
