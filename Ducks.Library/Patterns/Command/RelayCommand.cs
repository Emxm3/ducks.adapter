﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Library.Patterns.Command
{
    public class RelayCommand : IRelayCommand
    {
        private readonly Action<object> _action;
        private readonly Action _actionEmpty;

        public event EventHandler CanExecuteChanged = delegate { };

        public RelayCommand(Action<object> action)
        {
            _action = action;
        }
        public RelayCommand(Action action)
        {
            _actionEmpty = action;
        }

        public bool CanExecute(object parameter)
        {
            //you should write the logic where it's required before running the command I say. so return true.
            return true;
        }

        public void Execute(object parameter)
        {
            _action?.Invoke(parameter);
            _actionEmpty?.Invoke();

        }
    }

    public interface IRelayCommand : ICommand
    {

    }
}
