﻿using Ducks.Adapter.MVVM.Model.Containers;
using Ducks.Library.Model;

namespace Ducks.Library.Patterns.Factory
{
    public class AdaptingDuckFactory : IDuckFactory
    {
        private ICurrentDuckContainer currentDuckContainer;

        public AdaptingDuckFactory(ICurrentDuckContainer currentDuckContainer)
        {
            this.currentDuckContainer = currentDuckContainer;
        }
        public IDuck Get()
        {

            return currentDuckContainer.Value.Clone() as IDuck; //Clone it. we don't want shared references to ensure that if we change one duck, the other ducks don't get changed
        }
    }
}
