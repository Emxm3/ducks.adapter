﻿namespace Ducks.Library.Patterns.Factory
{
    public interface IFactory<TGet> where TGet : class
    {
        TGet Get();
    }

    public interface IFactory<TGet, T> where TGet : class
    {
        TGet Get(T id);
    }
}