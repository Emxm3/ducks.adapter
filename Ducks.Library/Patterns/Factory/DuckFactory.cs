﻿using Ducks.Library.Attributes;
using Ducks.Library.Model;
using Ducks.Library.Model.Ducks;
using System.Collections.Generic;
using System.Linq;

namespace Ducks.Library.Patterns.Factory
{
    [AutoWire(AsSingleton:true)]
    public class DuckFactory : IDuckFactory
    {
        private readonly IEnumerable<IDuck> ducks;

        public DuckFactory(IEnumerable<IDuck> ducks)
        {
            this.ducks = ducks;
        }

        public IDuck Get()
        {
            var duck = ducks.FirstOrDefault(d => d.GetType() == typeof(Mallard));

            return duck.Clone() as IDuck;
        }
    }

    public interface IDuckFactory : IFactory<IDuck>
    {
    }
}
