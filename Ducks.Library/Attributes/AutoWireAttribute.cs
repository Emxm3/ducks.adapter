﻿using System;

namespace Ducks.Library.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class AutoWireAttribute : Attribute
    {
        public bool AsSingleton { get; } //When autowiring, wire this up as a singleton
        public AutoWireAttribute(bool AsSingleton = false)
        {
            this.AsSingleton = AsSingleton;
        }
    }
}
