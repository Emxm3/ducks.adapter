﻿using Ducks.Adapter.MVVM.Model;
using Ducks.Adapter.MVVM.Model.Containers;
using Ducks.Adapter.MVVM.ViewModel.Catalogue;
using Ducks.Library.Attributes;
using Ducks.Library.Model;
using Ducks.Library.Model.Ducks;
using Ducks.Library.Model.Ponds;
using Ducks.Library.Observable;
using External.Ponds;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ducks.Adapter.MVVM.ViewModel.MainWindow
{


    [AutoWire(AsSingleton: true)]
    public class MainWindowViewModel : ObservableClass, IMainWindowViewModel
    {
        private ObservableCollection<IPond> _ListOfPonds;
        private bool _Loaded = false;
        private ObservableCollection<IDuck> _ListOfDucks;
        private ObservableCollection<ICatalogueDuck> _DuckCatalouge;
        private readonly IEnumerable<IPond> ponds;
        private readonly IEnumerable<IDuck> ducks;
        private readonly Func<IDuck, ICatalogueDuck> catalogueDuckFactory;
        private readonly ICurrentDuckContainer currentDuckContainer;
        private IDuck _TargetDuck;

        public ObservableCollection<IPond> ListOfPonds { get => _ListOfPonds ??= new(); set => SetPropertyValue(ref _ListOfPonds, value); }
        public ObservableCollection<IDuck> ListOfDucks { get => _ListOfDucks ??= new(); set => SetPropertyValue(ref _ListOfDucks, value); }

        public ObservableCollection<ICatalogueDuck> DuckCatalouge { get => _DuckCatalouge ??= new(); set => SetPropertyValue(ref _DuckCatalouge, value); }

        public ICommand Initialize => new ActionCommand(() => _Initialize());
        public ICommand Clear => new ActionCommand(() => _Clear());
        private void _Clear()
        {
            ListOfDucks.Clear();
            NotifyPropertyChanged(nameof(ListOfDucks));
        }

        public bool Loaded { get => _Loaded; set => SetPropertyValue(ref _Loaded, value); }
        public IMagicalFarAwayLand MagicalFarAwayLand { get; }
        public IFarm Farm { get; }
        public IDuck TargetDuck { get => _TargetDuck; set => SetPropertyValue(ref _TargetDuck, value, () => currentDuckContainer.Value = value); }

        private void _Initialize()
        {
            DuckCatalouge = new(ducks.Select(catalogueDuckFactory));
            DuckCatalouge.ToList().ForEach(d => d.OnDuckSelected += OnDuckSelected);
            //LISTEN TO ALL EVENTS:
            foreach (var pond in ponds)
                pond.EmitDuck += GetDuck;

            MagicalFarAwayLand.OnCreation += GetDuck;
            Farm.OnTransmit += GetDuck;

            Task.Factory.StartNew(LoadPonds); //Load the ducks, then hide the loading bar

            TargetDuck = ducks.FirstOrDefault(d => d.GetType() == typeof(Mallard)); //We want to look for Mallards
        }

        private void OnDuckSelected(object sender, IDuck e)
        {
            TargetDuck = e;
        }

        private void LoadPonds()
        {
            //Update their Strategies. This current implementation is bad design but we'll deal with it later.
            ListOfPonds.ToList().ForEach(p => { });
            NotifyPropertyChanged(nameof(ListOfPonds));
            Loaded = true;
        }

        public MainWindowViewModel(
               IEnumerable<IPond> ponds
            , IMagicalFarAwayLand magicalFarAwayLand
            , IFarm farm
            , IEnumerable<IDuck> ducks
            , Func<IDuck, ICatalogueDuck> catalogueDuckFactory
            , ICurrentDuckContainer currentDuckContainer)
        {

            //Notice we are only storing dependencies with minor transforms. 
            //We don't want to instantiate or execute any of our dependencies during this step
            ListOfPonds = new ObservableCollection<IPond>(ponds);
            this.ponds = ponds;
            MagicalFarAwayLand = magicalFarAwayLand;
            Farm = farm;
            this.ducks = ducks;
            this.catalogueDuckFactory = catalogueDuckFactory;
            this.currentDuckContainer = currentDuckContainer;
        }

        /// <summary>
        /// Executes when a source event is raised and broadcasts a duck
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="duck"></param>
        private void GetDuck(object sender, IDuck duck)
        {
            ListOfDucks.Add(duck);
            NotifyPropertyChanged(nameof(ListOfDucks));
        }
    }

    public interface IMainWindowViewModel
    {
        ObservableCollection<ICatalogueDuck> DuckCatalouge { get; set; }
        IFarm Farm { get; }
        ICommand Initialize { get; }
        ObservableCollection<IDuck> ListOfDucks { get; set; }
        ObservableCollection<IPond> ListOfPonds { get; set; }
        bool Loaded { get; set; }
        IMagicalFarAwayLand MagicalFarAwayLand { get; }
        IDuck TargetDuck { get; set; }
    }
}
