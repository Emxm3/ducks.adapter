﻿using Ducks.Library.Attributes;
using Ducks.Library.Model;
using Ducks.Library.Observable;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.Windows.Input;

namespace Ducks.Adapter.MVVM.ViewModel.Catalogue
{
    [AutoWire]
    public class CatalogueDuck : ObservableClass, ICatalogueDuck
    {
        private IDuck _Duck;

        // The type of duck this item catalogues
        public IDuck Duck { get => _Duck; set => SetPropertyValue(ref _Duck, value); }

        //The action to perform when selected
        public ICommand Selected => new ActionCommand(() => OnDuckSelected?.Invoke(this,Duck));

        public event EventHandler<IDuck> OnDuckSelected;
    }

    public interface ICatalogueDuck
    {
        event EventHandler<IDuck> OnDuckSelected;
        IDuck Duck { get; set; }
        ICommand Selected { get; }
    }
}
