﻿using System.Windows.Controls;

namespace Ducks.Adapter.MVVM.View
{
    /// <summary>
    /// Interaction logic for PondView.xaml
    /// </summary>
    public partial class PondView : UserControl
    {
        public PondView()
        {
            InitializeComponent();
        }
    }
}
