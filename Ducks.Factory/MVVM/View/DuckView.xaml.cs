﻿using System.Windows.Controls;

namespace Ducks.Adapter.MVVM.View
{
    /// <summary>
    /// Interaction logic for DuckView.xaml
    /// </summary>
    public partial class DuckView : UserControl
    {
        public DuckView()
        {
            InitializeComponent();
        }
    }
}
