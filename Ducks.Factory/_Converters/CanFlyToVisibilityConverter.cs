﻿using Ducks.Adapter._wpf;
using Ducks.Library.Model.Behaviour;
using System;
using System.Globalization;
using System.Windows;

namespace Ducks.Adapter._Converters
{
    public class CanFlyToVisibilityConverter : BaseConverter<CanFlyToVisibilityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value is ICanFly ? Visibility.Visible : Visibility.Collapsed;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
