﻿using Ducks.Adapter._wpf;
using System;
using System.Collections;
using System.Globalization;
using System.Windows;

namespace Ducks.Adapter._Converters
{
    public class EmptyIEnumerableToVisibilityConverter : BaseConverter<EmptyIEnumerableToVisibilityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is IEnumerable enumerable && enumerable.GetEnumerator().MoveNext()) //Can we move to first item? This isdicates there are items
                return Visibility.Visible;

            return Visibility.Hidden; //Hide the element because the collection is empty
            
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
