﻿using Ducks.Adapter._wpf;
using System;
using System.Globalization;
using System.Windows;

namespace Ducks.Adapter._Converters
{
    public class LoadedToVisibilityConverter : BaseConverter<LoadedToVisibilityConverter>
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool loaded)
                return loaded ? Visibility.Hidden : Visibility.Visible;

            return Visibility.Visible;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
