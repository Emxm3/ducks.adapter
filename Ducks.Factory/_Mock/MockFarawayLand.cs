﻿
using Ducks.Library.Model;
using External.Ponds;
using System;
using System.Windows.Input;

namespace Ducks.Adapter._Mock
{
    internal class MockFarawayLand : IMagicalFarAwayLand
    {
        public string Land => "Mock Land";

        public ICommand MagicallyAppear => throw new NotImplementedException();

        public event EventHandler<IDuck> OnCreation;

        public void ManifestDuckFromRainbows()
        {
        }
    }
}