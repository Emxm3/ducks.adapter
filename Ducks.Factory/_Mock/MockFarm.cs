﻿using Ducks.Library.Model;
using External.Ponds;
using System;
using System.Windows.Input;

namespace Ducks.Adapter._Mock
{
    public class MockFarm : IFarm
    {
        public string Title => "Mock Farm";

        public ICommand TransmitDuck => throw new NotImplementedException();

        public event EventHandler<IDuck> OnTransmit;
    }
}