﻿
using Ducks.Adapter.MVVM.ViewModel.Catalogue;
using Ducks.Adapter.MVVM.ViewModel.MainWindow;
using Ducks.Library.Model;
using Ducks.Library.Model.Ponds;
using Ducks.Library.Observable;
using External.Ponds;
using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Ducks.Adapter._Mock
{
    public class MockMainWindowViewModel : ObservableClass, IMainWindowViewModel
    {
        private ObservableCollection<IPond> _ListOfPonds;
        private IDuck _CurrentDuck;

        public IDuck CurrentDuck { get => _CurrentDuck; set => SetPropertyValue(ref _CurrentDuck, value); }



        public ICommand Initialize => throw new NotImplementedException();

        public bool Loaded => false;

        public ObservableCollection<IPond> ListOfPonds { get => _ListOfPonds ??= new(); set => SetPropertyValue(ref _ListOfPonds, value); }
        public ObservableCollection<ICatalogueDuck> DuckCatalouge { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IFarm Farm => new MockFarm();

        public ObservableCollection<IDuck> ListOfDucks { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        bool IMainWindowViewModel.Loaded { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public IMagicalFarAwayLand MagicalFarAwayLand => new MockFarawayLand();

        public IDuck TargetDuck { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public MockMainWindowViewModel()
        {
            
        }
    }
}
