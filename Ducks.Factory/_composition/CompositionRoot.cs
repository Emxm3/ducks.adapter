﻿using Ducks.Adapter.MVVM.Model;
using Ducks.Adapter.MVVM.ViewModel.Catalogue;
using Ducks.Adapter.MVVM.ViewModel.MainWindow;
using Ducks.Library.Model;
using Ducks.Library.Model.Ponds;
using External.Ponds;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Ducks.Adapter._composition
{
    public class CompositionRoot : BaseRoot
    {
        [STAThread]
        public static void Main()
        {
            new CompositionRoot().Execute<MainWindow, IMainWindowViewModel>();
        }

        public override void CustomInjections(Container container)
        {

            container.RegisterInstance<Func<IDuck, ICatalogueDuck>>((d) => 
            {
                ICatalogueDuck cat = container.GetInstance<ICatalogueDuck>();
                cat.Duck = d;
                return cat;
            });

            container.RegisterSingleton<IMagicalFarAwayLand, MagicalFarAwayLand>();
            container.RegisterSingleton<IFarm, Farm>();

            RegisterAllAsSingleton<IDuck>(container);
            RegisterAllAsSingleton<IPond>(container);
        }

    }
}
