﻿
using Ducks.Library.Attributes;
using Ducks.Library.Model;
using Ducks.Library.Patterns.Command;
using Ducks.Library.Patterns.Factory;
using System;
using System.Windows.Input;

namespace External.Ponds
{
    [AutoWire(AsSingleton:true)]
    public class MagicalFarAwayLand : IMagicalFarAwayLand
    //Unicorns and kittens!
    {
        private readonly IDuckFactory factory;

        public event EventHandler<IDuck> OnCreation;

        public ICommand MagicallyAppear => new RelayCommand(() => _MagicallyAppear());

        public string Land => "~*~ Magical Far Away Land ~*~";

        public MagicalFarAwayLand(IDuckFactory factory)
        {
            this.factory = factory;
        }


        private void _MagicallyAppear()
        {
            ManifestDuckFromRainbows();
        }

        public void ManifestDuckFromRainbows()
        {
            IDuck magicDuck = factory.Get();

            OnCreation?.Invoke(this, magicDuck);
        }
    }

    public interface IMagicalFarAwayLand
    {
        public string Land { get; }
        event EventHandler<IDuck> OnCreation;
        ICommand MagicallyAppear { get; }

        void ManifestDuckFromRainbows();
    }
}
