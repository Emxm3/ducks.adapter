﻿using Ducks.Library.Attributes;
using Ducks.Library.Model;
using Ducks.Library.Patterns.Command;
using Ducks.Library.Patterns.Factory;

using System;
using System.Windows.Input;

namespace External.Ponds
{
    [AutoWire(AsSingleton: true)]
    public class Farm : IFarm
    // This is not a pond (neither are a few others, but this is to show how to handle across unrelated types)
    {
        private readonly IDuckFactory duckFactory;

        public event EventHandler<IDuck> OnTransmit;
        public ICommand TransmitDuck => new RelayCommand(FarmDuck);

        public string Title => "Incompetent Farm";

        public Farm(IDuckFactory duckFactory)
        {
            this.duckFactory = duckFactory;
        }


        private void FarmDuck()
        {
            IDuck duck = duckFactory.Get();

            OnTransmit?.Invoke(this, duck);
        }

    }

    public interface IFarm
    {
        string Title { get; }
        ICommand TransmitDuck { get; }
        event EventHandler<IDuck> OnTransmit;
    }
}
